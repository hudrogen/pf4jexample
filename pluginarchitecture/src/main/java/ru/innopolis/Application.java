package ru.innopolis;



import org.pf4j.DefaultPluginManager;
import org.pf4j.PluginManager;
import org.pf4j.PluginWrapper;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class Application {

    public static void main(String[] args) {

        PluginManager pluginManager = new DefaultPluginManager(Paths.get("../plugins"));
        //PluginManager pluginManager = new DefaultPluginManager();
        pluginManager.loadPlugins();
        pluginManager.startPlugins();
        System.out.println("Путь к папке с плагинами: " + System.getProperty("pf4j.pluginsDir", "plugins"));
        List<PluginWrapper> plugins = pluginManager.getStartedPlugins();
        for (PluginWrapper plugin : plugins) {
            System.out.println(plugin.getDescriptor().getPluginId());
        }
    }
}
