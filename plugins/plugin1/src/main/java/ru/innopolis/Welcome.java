package ru.innopolis;

import org.pf4j.Plugin;
import org.pf4j.PluginException;
import org.pf4j.PluginWrapper;

public class Welcome extends Plugin {

    public Welcome(PluginWrapper wrapper) {
        super(wrapper);
    }

    @Override
    public void start() throws PluginException {
        System.out.println("Plugin1 starts");
    }

    @Override
    public void stop() throws PluginException {
        System.out.println("Plugin1 stops");
    }
}
