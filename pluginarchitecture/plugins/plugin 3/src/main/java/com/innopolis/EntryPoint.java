package com.innopolis;

import org.pf4j.*;

public class EntryPoint extends Plugin{

    public EntryPoint(PluginWrapper wrapper) {
        super(wrapper);
    }

    @Override
    public void start() throws PluginException {
        System.out.println("Plugin #3 starts");
    }

    @Override
    public void stop() throws PluginException {
        System.out.println("Plugin #3 stops");
    }

    public String getSomeString(){
        return "SomeString";
    }

    @Extension
    public static class WelcomeGreeting {

        public String getGreeting() {
            return "Welcome";
        }

    }


}
